import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from "@angular/forms";

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { AComponent } from './components/a/a.component';
import { BComponent } from './components/b/b.component';
import { CComponent } from './components/c/c.component';

import { DemoService } from "../service/demo";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { LayoutModule } from "ng2-flex-layout";
import { MatCardModule,MatButtonModule } from "@angular/material";
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AComponent,
    BComponent,
    CComponent
  ],
  imports: [
    BrowserModule,FormsModule,BrowserAnimationsModule,MatCardModule,LayoutModule,MatButtonModule
  ],
  providers: [DemoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
