import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DemoService } from "../../../service/demo";
@Component({
  selector: 'app-a',
  templateUrl: './a.component.html',
  styleUrls: ['./a.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AComponent implements OnInit {
  result : number
  constructor(private demoService : DemoService) {
    this.demoService.getResult().subscribe(n=>{
      this.result = n;
      console.log('Component A :',n)
    })
  }

  ngOnInit() {
  }

}
