import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DemoService } from "../../../service/demo";
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
  public numberA : number = 0;
  public numberB : number = 0;
  constructor(private demoService : DemoService) {
    
  }

  count = (a:number,b:number)=>{
    let result = a + b;
    console.log(result)
    this.demoService.setResult(result);
  }

  ngOnInit() {
  }

}
