import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DemoService } from "../../../service/demo";
import { OnDestroy } from '@angular/core/src/metadata/lifecycle_hooks';
import { Subscription } from "rxjs/Subscription";
@Component({
  selector: 'app-c',
  templateUrl: './c.component.html',
  styleUrls: ['./c.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CComponent implements OnInit {
  result : number
  constructor(private demoService : DemoService) {
    this.demoService.getResult().subscribe(n=>{
      this.result = n;
      console.log('Component C :',n)
    })
  }

  ngOnInit() {
  }

}
