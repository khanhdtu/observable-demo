import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DemoService } from "../../../service/demo";
@Component({
  selector: 'app-b',
  templateUrl: './b.component.html',
  styleUrls: ['./b.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BComponent implements OnInit {
  result : number
  constructor(private demoService : DemoService) {
    this.demoService.getResult().subscribe(n=>{
      this.result = n;
      console.log('Component B :',n)
    })
  }

  ngOnInit() {
  }

}
