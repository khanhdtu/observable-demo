import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Subject } from "rxjs/Subject";
@Injectable()
export class DemoService{
    result = new Subject<number>();
    setResult = (n:number) => {
        this.result.next(n);
    }
    getResult = () : Observable<any> => {
        return this.result.asObservable();
    }
}